<?php

namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet {
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $trajets = (new TrajetRepository)->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('/vueGenerale.php', ['trajets' => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php"]);
    }

    public static function afficherDetail() : void {
        $id = $_GET['id'];
        $trajet = (new TrajetRepository)->recupererParClePrimaire($id); //appel au modèle pour gérer la BD
        if ($trajet === null) {
            self::afficherVue('vueGenerale.php', ["titre" => "Detail trajet", "cheminCorpsVue" => "trajet/erreur.php", "messageErreur" => "Trajet inconnu"]);
        } else {
            self::afficherVue('vueGenerale.php', ['trajet' => $trajet, "titre" => "Detail trajet", "cheminCorpsVue" => "trajet/detail.php"]);
        }
    }

    public static function afficherErreur(string $messageErreur = ""): void {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "trajet/erreur.php", "messageErreur" => $messageErreur]);
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Detail trajet", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $id = null;
        $depart = $_GET["depart"];
        $arrivee = $_GET["arrivee"];
        $date = new DateTime($_GET["date"]);
        $prix = $_GET["prix"];
        $conducteur = (new UtilisateurRepository)->recupererParClePrimaire($_GET["conducteurLogin"]);
        $nonFumeur = isset($_GET["nonFumeur"]);
        $trajet = new Trajet($id, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = TrajetRepository::recupererTrajets();
        self::afficherVue('vueGenerale.php', ['trajets' => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/trajetCree.php", "id" => $id]);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
        self::afficherVue('vueGenerale.php', ["titre" => "Mise a jour trajet", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php", "trajet" => $trajet]);
    }

    public static function mettreAJour(): void
    {
        $id = $_GET['id'];
        $depart = $_GET['depart'];
        $arrivee = $_GET['arrivee'];
        $date = new DateTime($_GET['date']);
        $prix = $_GET['prix'];
        $conducteur = (new UtilisateurRepository)->recupererParClePrimaire($_GET['conducteurLogin']);
        $nonFumeur = isset($_GET['nonFumeur']);

        $trajet = new Trajet($id, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);
        (new TrajetRepository)->mettreAJour($trajet);

        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['trajets' => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/trajetMisAJour.php", "id" => $id]);
    }

    public static function supprimer(): void
    {
        $id = $_GET['id'];
        (new TrajetRepository)->supprimerParLogin($id);
        $trajets = TrajetRepository::recupererTrajets();
        self::afficherVue('vueGenerale.php', ['trajets' => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/trajetSupprime.php", "id" => $id]);
    }

    /**
     * @return array
     * @throws \Exception
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $depart = $tableauDonneesFormulaire["depart"];
        $arrivee = $tableauDonneesFormulaire["arrivee"];
        $date = new DateTime($tableauDonneesFormulaire["date"]);
        $prix = $tableauDonneesFormulaire["prix"];
        $conducteur = (new UtilisateurRepository)->recupererParClePrimaire($tableauDonneesFormulaire["conducteurLogin"]);
        $nonFumeur = isset($tableauDonneesFormulaire["nonFumeur"]);
        return array($id, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);
    }
}
?>
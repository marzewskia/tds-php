<?php
/** @var Trajet[] $trajets */

use App\Covoiturage\Modele\DataObject\Trajet;

foreach ($trajets as $trajet) {
    $idHTML = htmlspecialchars($trajet->getId());
    $idURL = rawurlencode($trajet->getId());
    echo '<p> Trajet de id : <strong>' . $idHTML . '</strong>. ';
    echo '<a href="controleurFrontal.php?controleur=trajet&action=afficherDetail&id=' . $idURL . '">Voir le détail</a> ';
    echo '<a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=' . $idURL . '">Mettre à jour</a> ';
    echo '<a href="controleurFrontal.php?controleur=trajet&action=supprimer&id=' . $idURL . '">Supprimer</a></p>';
}
echo '<p> <a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation' . '"><strong>Créer trajet</strong></a></p>';
?>

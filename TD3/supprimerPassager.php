<?php
require_once 'Trajet.php';
require_once 'Utilisateur.php';

// Récupérer les paramètres de l'URL
$login = $_GET['login'] ?? null;
$trajetId = $_GET['trajetid'] ?? null;

// Vérifier si les paramètres sont présents
if ($login === null || $trajetId === null) {
    echo "Erreur : login et trajet_id sont requis.";
    exit;
}

// Créer un objet Trajet avec seulement l'ID
$trajet = new Trajet(
    $trajetId,
    null,
    null,
    null,
    null,
    null,
    null
);

// Supprimer le passager
$resultat = $trajet->supprimerPassager($login);

if ($resultat) {
    echo "Le passager avec le login '$login' a été désinscrit du trajet $trajetId avec succès.";
} else {
    echo "Erreur lors de la désinscription du passager.";
}

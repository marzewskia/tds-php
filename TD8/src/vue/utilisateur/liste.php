<?php
/** @var Utilisateur[] $utilisateurs */

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<p> Utilisateur de Login <strong>' . $loginHTML . '</strong>. ';
    echo '<a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginURL . '">Voir le détail</a> ';
    if (ConnexionUtilisateur::estConnecte() && ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
        echo '<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL . '">Mettre à jour</a> ';
        echo '<a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . $loginURL . '">Supprimer</a></p>';
    }
}
echo '<p> <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation' . '"><strong>Créer utilisateur</strong></a></p>';
?>
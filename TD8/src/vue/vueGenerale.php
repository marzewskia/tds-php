<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../ressources/style.css">
    <title>
        <?php
        /**
         * @var string $titre
         */
        echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src= "../../ressources/heart.png"></a>
            </li>
            <?php
            use App\Covoiturage\Lib\ConnexionUtilisateur;
            if (!ConnexionUtilisateur::estConnecte()) {
                echo '<li><a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur"><img src= "../../ressources/add-user.png"></a></li>';
                echo '<li><a href="controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur"><img src= "../../ressources/enter.png"></a></li>';
            } else {
                echo '<li><a href="controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=' . rawurlencode($_SESSION['_utilisateurConnecte']) . '"><img src="../../ressources/user.png"></a></li>';
                echo '<li><a href="controleurFrontal.php?action=deconnecter&controleur=utilisateur"><img src= "../../ressources/logout.png"></a></li>';
            }
            ?>
        </ul>
    </nav>
</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage d'Antoni
    </p>
</footer>
</body>
</html>
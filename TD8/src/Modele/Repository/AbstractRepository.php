<?php

namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use Exception;

abstract class AbstractRepository
{
    public function mettreAJour(AbstractDataObject $objet): void
    {
        $colonnes = $this->getNomsColonnes();
        $tags = array_map(function($colonne) { return "{$colonne} = :{$colonne}"; }, $colonnes);
        $clePrimaire = $this->getNomClePrimaire();

        $sql = "UPDATE " . $this->getNomTable() . " SET " . implode(', ', $tags) . " WHERE {$clePrimaire} = :{$clePrimaire}";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);

        foreach ($colonnes as $colonne) {
            $pdoStatement->bindValue(":{$colonne}", $values[$colonne]);
        }

        $pdoStatement->execute();
    }


    public function ajouter(AbstractDataObject $objet): bool
    {
        $colonnes = $this->getNomsColonnes();
        $tags = array_map(function($colonne) { return ":{$colonne}"; }, $colonnes);

        try {
            $sql = "INSERT INTO " . $this->getNomTable() . " (" . implode(',', $colonnes) . ") VALUES(" . implode(',', $tags) . ")";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            $values = $this->formatTableauSQL($objet);

            foreach ($colonnes as $colonne) {
                $pdoStatement->bindValue(":{$colonne}", $values[$colonne]);
            }

            $pdoStatement->execute();
            return true;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function supprimerParLogin($valeurClePrimaire): void
    {
        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :valeurClePrimaireTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "valeurClePrimaireTag" => $valeurClePrimaire
        );
        $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * from " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . "= :clePrimaireTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "clePrimaireTag" => $clePrimaire,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $objetFormatTableau = $pdoStatement->fetch();

        if (!$objetFormatTableau) {
            return null;
        } else {
            return $this->construireDepuisTableauSQL($objetFormatTableau);
        }
    }

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM " . $this->getNomTable());
        $objets = [];
        foreach ($pdoStatement as $objetFormatTableau) {
            $objets[] = $this->construireDepuisTableauSQL($objetFormatTableau);
        }
        return $objets;
    }

    protected abstract function getNomTable(): string;

    protected abstract function getNomClePrimaire(): string;

    /** @return string[] */
    protected abstract function getNomsColonnes(): array;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;
}
<?php

namespace App\Covoiturage\Lib;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        $_SESSION[self::$cleConnexion] = $loginUtilisateur;
    }

    public static function estConnecte(): bool
    {
        return isset($_SESSION[self::$cleConnexion]);
    }

    public static function deconnecter(): void
    {
        unset($_SESSION[self::$cleConnexion]);
    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        return $_SESSION[self::$cleConnexion];
    }

    public static function estUtilisateur($login): bool
    {
        return self::getLoginUtilisateurConnecte() === $login;
    }

}


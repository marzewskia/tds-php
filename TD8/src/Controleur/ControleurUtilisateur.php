<?php

namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('/vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository)->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
        if ($utilisateur === null) {
            self::afficherVue('vueGenerale.php', ["titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => "Utilisateur inconnu"]);
        } else {
            self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Creation utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function afficherFormulaireConnexion(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Connexion", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter(): void{
        if (!isset($_GET['login']) || !isset($_GET['mdp'])) {
            self::afficherErreur("Login et/ou mot de passe manquant");
        }
        $login = $_GET['login'];
        $mdp = $_GET['mdp'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur === null) {
            self::afficherErreur("Login et/ou mot de passe incorrect.");
        }
        $mdpCorrect = MotDePasse::verifier($mdp, $utilisateur->getMdpHache());
        if (!$mdpCorrect) {
            echo $utilisateur->getMdpHache();
            echo $mdp;
            self::afficherErreur("Login et/ou mot de passe incorrect.");
        }
        ConnexionUtilisateur::connecter($login);
        self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php"]);
    }

    public static function deconnecter(): void
    {
        ConnexionUtilisateur::deconnecter();
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php"]);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        if (!ConnexionUtilisateur::estConnecte() || !ConnexionUtilisateur::estUtilisateur($_GET['login'])) {
            self::afficherErreur("Vous n'etez pas connecte en tant que utilisateur que vous voulez modifier");
            return;
        }
        $utilisateur = (new UtilisateurRepository)->recupererParClePrimaire($_GET['login']);

        self::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php", "utilisateur" => $utilisateur]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $login = $_GET['login'];
        $prenom = $_GET['prenom'];
        $nom = $_GET['nom'];
        $mdp = $_GET['mdp'];
        $mdp2 = $_GET['mdp2'];
        if ($mdp !== $mdp2) {
            self::afficherErreur("Les mots de passe ne sont pas identiques");
            return;
        }
        $utilisateur = new Utilisateur($login, $prenom, $nom, MotDePasse::hacher($mdp));
        (new UtilisateurRepository)->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "login" => $login]);
    }


    public static function mettreAJour(): void
    {
        // chaps obligatoires
        if (!isset($_GET['login']) || !isset($_GET['prenom']) || !isset($_GET['nom']) || !isset($_GET['mdpAncien']) || !isset($_GET['mdp']) || !isset($_GET['mdpConfirmation'])) {
            self::afficherErreur("Tous les champs obligatoires doivent être remplis");
            return;
        }

        $login = $_GET['login'];
        $prenom = $_GET['prenom'];
        $nom = $_GET['nom'];
        $mdpAncien = $_GET['mdpAncien'];
        $mdp = $_GET['mdp'];
        $mdpConfirmation = $_GET['mdpConfirmation'];

        // login existe
        $utilisateurRepository = new UtilisateurRepository();
        $utilisateur = $utilisateurRepository->recupererParClePrimaire($login);
        if ($utilisateur === null) {
            self::afficherErreur("Ce login n'existe pas");
            return;
        }

        // memes mots de passe
        if ($mdp !== $mdpConfirmation) {
            self::afficherErreur("Les nouveaux mots de passe ne correspondent pas");
            return;
        }

        // ancien mdp correct
        if (!MotDePasse::verifier($mdpAncien, $utilisateur->getMdpHache())) {
            self::afficherErreur("L'ancien mot de passe est incorrect");
            return;
        }

        // utilisateur connecte est celui modifie
        if (!ConnexionUtilisateur::estConnecte() || !ConnexionUtilisateur::estUtilisateur($login)) {
            self::afficherErreur("Vous n'êtes pas autorisé à modifier cet utilisateur");
            return;
        }

        $utilisateurMisAJour = new Utilisateur($login, $prenom, $nom, MotDePasse::hacher($mdp));
        $utilisateurRepository->mettreAJour($utilisateurMisAJour);

        $utilisateurs = $utilisateurRepository->recuperer();
        self::afficherVue('vueGenerale.php', [
            'utilisateurs' => $utilisateurs,
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php",
            "login" => $login
        ]);
    }


    public static function afficherErreur(string $messageErreur = ""): void {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }

    public static function supprimer(): void
    {
        $login = $_GET['login'];
        (new UtilisateurRepository)->supprimerParLogin($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", "login" => $login]);
    }

    /**
     * @return array
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $nom = $tableauDonneesFormulaire['nom'];
        $mdp = $tableauDonneesFormulaire['mdp'];
        return (new Utilisateur($login, $prenom, $nom, $mdp));
    }

    public static function testerSession(): void
    {
        $session = Session::getInstance();
        $session->enregistrer("testNom", "testValeur");
        echo $session->lire("testNom");
    }

    /*
    public static function deposerCookie($cle, $valeur, $expiration): void
    {
        Cookie::enregistrer($cle, $valeur, $expiration);
    }

    public static function lireCookie($cle): ?string
    {
        return Cookie::lire($cle);
    }

    public static function supprimerCookie($cle): void
    {
        Cookie::supprimer($cle);
    }
    */
}
?>
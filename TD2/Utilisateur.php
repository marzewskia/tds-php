<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // Getters
    public function getNom(): string {
        return $this->nom;
    }

    public function getLogin(): string
    {
        return substr($this->login, 0, 64);
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    // Setters
    public function setNom(string $nom): void {
        $this->nom = substr($nom, 0, 64);
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login): void
    {
        $this->login = substr($login, 0, 64);
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($utilisateurFormatTableau["loginBaseDeDonnees"], $utilisateurFormatTableau["prenomBaseDeDonnees"], $utilisateurFormatTableau["nomBaseDeDonnees"]);
    }

    public static function recupererUtilisateurs() {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $utilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }

    // Un constructeur
    public function __construct(string $login, string $nom, string $prenom) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string {
        return "Login: " . $this->login . "| Nom: " . $this->nom . "| Prenom: " . $this->prenom;
    }


}
?>

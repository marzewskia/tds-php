<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>

    <body>
        Voici le résultat du script PHP :
        <?php
            // Ceci est un commentaire PHP sur une ligne EX5 TD2
            /* Ceci est le 2ème type de commentaire PHP
            sur plusieurs lignes */

            // On met la chaine de caractères "hello" dans la variable 'texte'
            // Les noms de variable commencent par $ en PHP
            echo "<br>";
            $texte = "hello world !";

            // On écrit le contenu de la variable 'texte' dans la page Web
            echo $texte;

            $nom = "Marzewski";
            $prenom = "Antoni";
            $login = "marzesky";

            echo "<br>";

            echo "<br><strong>String simple avec variables :</strong><br>";

            echo "Utilisateur $prenom $nom de login $login";

            $utilisateur1 = [
                    'prenom' => 'John',
                    'nom' => 'Smith',
                    'login' => 'jsmith'
            ];

            $utilisateur2 = [
                    'prenom' => 'Jean',
                    'nom' => 'Dupont',
                    'login' => 'jdupont'
            ];

            $utilisateur3 = [
                'prenom' => 'Jan',
                'nom' => 'Nowak',
                'login' => 'jnowak'
            ];

            echo "<br>";

            $utilisateurs = [
                '0' => $utilisateur1,
                '1' => $utilisateur2,
                '2' => $utilisateur3
            ];

            echo "<br><h3>Liste des utilisateurs :</h3><br>";

            if (empty($utilisateurs)) {
                echo "<br>La liste d'utilisateurs est vide";
            } else {
                foreach ($utilisateurs as $cle => $valeur){
                    echo "<ul>";
                    foreach ($valeur as $cle2 => $valeur2){
                        echo "<li>$cle2 : $valeur2 </li><br>";
                    }
                    echo "</ul>";
                }
            }

            $listevide = [];

            if (empty($listevide)) {
                echo "<br>listevide est vide";
            }

            echo "<br><br> vardump: <br>";
            var_dump($utilisateurs);
        ?>
    </body>
</html> 
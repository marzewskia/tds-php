<?php

namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\ModeleUtilisateur as ModeleUtilisateur;

class ControleurUtilisateur {
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('/vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login); //appel au modèle pour gérer la BD
        if ($utilisateur === null) {
            self::afficherVue('vueGenerale.php', ["titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => "Utilisateur inconnu"]);
        } else {
            self::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Detail utilisateur", "cheminCorpsVue" => "utilisateur/formulaireUtilisateur.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $login = $_GET['login'];
        $prenom = $_GET['prenom'];
        $nom = $_GET['nom'];
        $utilisateur = new ModeleUtilisateur($login, $prenom, $nom);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

    public static function afficherErreur(string $messageErreur = ""): void {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }
}
?>
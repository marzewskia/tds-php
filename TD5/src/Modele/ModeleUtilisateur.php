<?php

namespace App\Covoiturage\Modele;
use App\Covoiturage\Modele\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;


class ModeleUtilisateur
{

    private string $login;
    private string $nom;
    private string $prenom;

    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    // Getters
    public function getNom(): string
    {
        return $this->nom;
    }

    public function getLogin(): string
    {
        return substr($this->login, 0, 64);
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager === null) {
            $this->trajetsCommePassager = $this->recupererTrajetsCommePassager();
    }
        return $this->trajetsCommePassager;
    }

    // Setters
    public function setNom(string $nom): void
    {
        $this->nom = substr($nom, 0, 64);
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login): void
    {
        $this->login = substr($login, 0, 64);
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): ModeleUtilisateur
    {
        return new ModeleUtilisateur(
            $utilisateurFormatTableau["login"],
            $utilisateurFormatTableau["prenom"],
            $utilisateurFormatTableau["nom"]
        );
    }

    public static function recupererUtilisateurs()
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $utilisateurs = [];
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }

    // Un constructeur
    public function __construct(string $login, string $nom, string $prenom)
    {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    /*
    public function __toString(): string
    {
        return "Login: " . $this->login . "| Nom: " . $this->nom . "| Prenom: " . $this->prenom;
    }
    */

    public static function recupererUtilisateurParLogin(string $login): ?ModeleUtilisateur
    {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if (!$utilisateurFormatTableau) {
            return null;
        } else {
            return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
    }

    public function ajouter(): bool
    {
        try {
            $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES(:login, :nom, :prenom)";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            $values = array(
                "login" => $this->login,
                "nom" => $this->nom,
                "prenom" => $this->prenom,
            );
            $pdoStatement->execute($values);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager() : array {
        $sql = "SELECT T.* FROM trajet T 
            JOIN passager P ON P.trajetId = T.id 
            WHERE P.passagerLogin = :loginTag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $this->login,
        );

        $pdoStatement->execute($values);

        $trajets = [];
        while ($row = $pdoStatement->fetch()) {
            $trajets[] = Trajet::construireDepuisTableauSQL($row);
        }
        return $trajets;
    }
}

?>

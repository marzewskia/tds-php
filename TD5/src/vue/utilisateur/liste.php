<?php
/** @var ModeleUtilisateur[] $utilisateurs */
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<p> Utilisateur de Login ' . $loginHTML . '.</p>';
    echo '<p> <a href="controleurFrontal.php?action=afficherDetail&login=' . $loginURL . '">Voir le détail</a></p>';
}
echo '<p> <a href="controleurFrontal.php?action=afficherFormulaireCreation' . '">Creer utilisateur</a></p>';
?>
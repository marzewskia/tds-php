<?php
use App\Covoiturage\Controleur\ControleurUtilisateur;
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

// On récupère l'action passée dans l'URL
$action = isset($_GET['action']) ? $_GET['action'] : 'default';

// Vérifier si 'login' existe dans $_GET
$login = isset($_GET['login']) ? $_GET['login'] : null;

// Appel de la méthode statique $action de ControleurUtilisateur
ControleurUtilisateur::$action();
?>
<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;

class TrajetRepository extends AbstractRepository
{
    public function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $object): array
    {
        $trajet = $object;
        return [
            "id" => $trajet->getId(),
            "depart" => $trajet->getDepart(),
            "arrivee" => $trajet->getArrivee(),
            "date" => $trajet->getDate()->format('Y-m-d H:i:s'),
            "prix" => $trajet->getPrix(),
            "conducteurLogin" => $trajet->getConducteur()->getLogin(),
            "nonFumeur" => $trajet->getNonFumeur() ? 1 : 0
        ];
    }

    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["prix"],
            (new UtilisateurRepository)->recupererParClePrimaire($trajetTableau["conducteurLogin"]), // À changer
            $trajetTableau["nonFumeur"], // À changer ?
        );
        $trajet->setPassagers(TrajetRepository::recupererPassagers($trajet));
        return $trajet;
    }

    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = (new TrajetRepository)->construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public static function recupererPassagers(Trajet $trajet): array
    {
        $sql = "SELECT U.* 
                FROM passager P 
                JOIN utilisateur U 
                ON U.login = P.passagerLogin
                WHERE P.trajetId = :idTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idTag" => $trajet->getId(),
        );

        $pdoStatement->execute($values);

        $passagers = [];
        while ($row = $pdoStatement->fetch()) {
            $passagers[] = (new UtilisateurRepository)->construireDepuisTableauSQL($row);
        }
        return $passagers;
    }
}


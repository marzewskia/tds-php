<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurGenerique {
    protected static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function enregistrerPreference() {
        $controleur_choisi = $_GET["controleur_defaut"];
        PreferenceControleur::enregistrer($controleur_choisi);
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire preference", "cheminCorpsVue" => "preferenceEnregistree.php"]);
    }

    public static function afficherFormulairePreference() {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire preference", "cheminCorpsVue" => "formulairePreference.php"]);
    }
}
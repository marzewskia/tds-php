<?php
/** @var Trajet[] $trajet */

$idTML = htmlspecialchars($trajet->getId());
$departHTML = htmlspecialchars($trajet->getDepart());
$arriveeHTML = htmlspecialchars($trajet->getArrivee());
$dateHTML = htmlspecialchars($trajet->getDate()->format("d/m/Y"));
$prixHTML = htmlspecialchars($trajet->getPrix());
$conducteurHTML = htmlspecialchars($trajet->getConducteur()->getPrenom() . " " . $trajet->getConducteur()->getNom());

echo '<p> Trajet de depart: <strong>' . $departHTML . '</strong> Arrivee: <strong>' . $arriveeHTML . '</strong> Date: <strong>' . $dateHTML . '</strong> Prix: <strong>' . $prixHTML . '</strong> Conducteur: <strong>' . $conducteurHTML . '</strong>.</p>';
?>
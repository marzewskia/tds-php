<?php
/** @var Trajet $trajet */

$idHTML = htmlspecialchars($trajet->getId());
$departHTML = htmlspecialchars($trajet->getDepart());
$arriveeHTML = htmlspecialchars($trajet->getArrivee());
$dateHTML = htmlspecialchars($trajet->getDate()->format('Y-m-d'));
$prixHTML = htmlspecialchars($trajet->getPrix());
$conducteurLoginHTML = htmlspecialchars($trajet->getConducteur()->getLogin());
$nonFumeurChecked = $trajet->getNonFumeur() ? 'checked' : '';
?>

<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='controleur' value='trajet'>
    <input type='hidden' name='action' value='mettreAJour'>
    <input type='hidden' name='id' value='<?= $idHTML ?>'>
    <fieldset>
        <legend>Mettre à jour le trajet :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Départ&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= $departHTML ?>" name="depart" id="depart_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivée&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= $arriveeHTML ?>" name="arrivee" id="arrivee_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" value="<?= $dateHTML ?>" name="date" id="date_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="number" step="0.01" value="<?= $prixHTML ?>" name="prix" id="prix_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin_id">Login du conducteur&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= $conducteurLoginHTML ?>" name="conducteurLogin" id="conducteurLogin_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non fumeur</label>
            <input class="InputAddOn-field" type="checkbox" name="nonFumeur" id="nonFumeur_id" <?= $nonFumeurChecked ?>>
        </p>
        <p>
            <input type="submit" value="Mettre à jour le trajet" />
        </p>
    </fieldset>
</form>
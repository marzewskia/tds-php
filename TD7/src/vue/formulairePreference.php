<?php
use App\Covoiturage\Modele\HTTP\Cookie;

$utilisateurChoisi = '';
$trajetChoisi = '';

if (isset($_COOKIE["preferenceControleur"]) && Cookie::lire("preferenceControleur") === "utilisateur") {
    $utilisateurChoisi = 'checked';
} elseif (isset($_COOKIE["preferenceControleur"]) && Cookie::lire("preferenceControleur") === "trajet") {
    $trajetChoisi = 'checked';
}
?>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='action' value='enregistrerPreference'>
    <fieldset>
        <legend>Indiquez votre controleur par defaut :</legend>
        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?= $utilisateurChoisi ?>>
        <label for="utilisateurId">Utilisateur</label>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?= $trajetChoisi ?>>
        <label for="trajetId">Trajet</label>
        <p>
            <input type="submit" value="Créer le trajet" />
        </p>
    </fieldset>
</form>
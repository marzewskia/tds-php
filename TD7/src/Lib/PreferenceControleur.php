<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Cookie;

class PreferenceControleur {
    private static string $clePreference = "preferenceControleur";

    public static function enregistrer(string $preference) : void
    {
        Cookie::enregistrer(PreferenceControleur::$clePreference, $preference);
    }

    public static function lire() : string
    {
        $valeur = Cookie::lire(PreferenceControleur::$clePreference);
        return unserialize($valeur);
    }

    public static function existe() : bool
    {
        // À compléter
        return false;
    }

    public static function supprimer() : void
    {
        // À compléter
    }
}


<?php
require_once 'ControleurUtilisateur.php';

// On récupère l'action passée dans l'URL
$action = isset($_GET['action']) ? $_GET['action'] : 'default';

// Vérifier si 'login' existe dans $_GET
$login = isset($_GET['login']) ? $_GET['login'] : null;

// Appel de la méthode statique $action de ControleurUtilisateur
ControleurUtilisateur::$action();
?>
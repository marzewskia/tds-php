<?php
require_once(__DIR__ . '/../Modele/ModeleUtilisateur.php');
class ControleurUtilisateur {
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]);
    }

    public static function afficherDetail() : void {
        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login); //appel au modèle pour gérer la BD
        if ($utilisateur === null) {
            self::afficherVue('utilisateur/erreur.php');
        } else {
            self::afficherVue('utilisateur/detail.php', ['utilisateur' => $utilisateur]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('utilisateur/formulaireUtilisateur.php');
    }

    public static function creerDepuisFormulaire(): void
    {
        $login = $_GET['login'];
        $prenom = $_GET['prenom'];
        $nom = $_GET['nom'];
        $utilisateur = new ModeleUtilisateur($login, $prenom, $nom);
        $utilisateur->ajouter();
        self::afficherListe();
    }
}
?>